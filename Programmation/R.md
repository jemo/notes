# R programming language

## [Ressources en français](https://github.com/statnmap/frrrenchies)

## R script file

```R
# My first program in R Programming
myString <- "Hello, World!"

print ( myString)
```

`Rscript test.R`

## R Markdown

* [Le language R Markdown](https://www.fun-mooc.fr/c4x/UPSUD/42001S02/asset/RMarkdown.html)
## Variables

`ls()` list all the variables available in the workspace.

## [Data Frames](https://www.tutorialspoint.com/r/r_data_frames.htm)

`str()` Affiche la structure.

## Cartographie

* [ggspatial](https://paleolimbot.github.io/ggspatial/articles/ggspatial.html)
* [Cartographie avec R](https://rcarto.github.io/carto_avec_r/)
    * [Accessibilité et données OSM](https://rcarto.gitpages.huma-num.fr/santelocal/)
