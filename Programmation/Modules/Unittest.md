# Module de test unitaire

## Lancer tous les tests 
```shell
python -m unittest
```

# Sources
* [Python Unittest](https://www.youtube.com/watch?v=6tNS--WetLI)
* [Documentation Unittest](https://docs.python.org/3/library/unittest.html)
