# Virtual env
installation de package spécifique

# create new venv
```shell
python -m venv venv
```

# activate
```shell
source venv/bin/activate
```

# lister les packages
```shell
pip list
```

# creation requirements.txt
```shell
pip freeze > requirements.txt
```

# installer les packages dans requirements.txt
```shell
pip install -r requirements.txt
```

# desactiver l'env virtuel
```shell
deactivate
```

# Source 
- [Python Tutorial: VENV](https://www.youtube.com/watch?v=Kg1Yvry_Ydk)
