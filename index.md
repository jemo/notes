# Applications

* [VimWiki](Applications/VimWiki.md)
* [Newsboat](Applications/Newsboat.md)

# Programmation

* [Python](Programmation/Python.md)
* [R](Programmation/R.md)
* [Git](Programmation/Git.md)

# Technologie

* [Machine Learning](Technologie/Machine_Learning.md)
* [gemini](Technologie/gemini.md)
* [Android](Technologie/Android.md)
