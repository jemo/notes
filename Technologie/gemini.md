# _The modern web is terrible_

Le protocole gemini est sécurisé par défaut (equivalent https) 
Permet de n'afficher que du texte.
Les liens sont séparés du texte et sur leur propre ligne.

## Syntaxe gemini space

```gemini
# Titre 1
## Titre 2
### Titre 3

> citation

Liens externe
=> gemini://foo.bar Foo Bar
Liens interne
=> /foo/bar Foo Bar
```

## Navigateurs gemini

* Command line
    * [Amfora](https://github.com/makeworld-the-better-one/amfora)
* Visuel
    * [Lagrange](https://gmi.skyjake.fi/lagrange/)

## Serveur gemini

* [agate](https://github.com/mbrubeck/agate)

## Source 

* [gemini circumlunar space](https://gemini.circumlunar.space/) 
* [Video de Distrotube sur gemini](https://www.youtube.com/watch?v=Iq1k_FCWPXk)
* [wikipedia Gemini (protocol) ](https://en.wikipedia.org/wiki/Gemini_(protocol))
