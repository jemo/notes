# Différentes méthodes de Machine Learning

* Feature Engineering + Pre-Precessing
* Linear Models
* Neural Networks
* Deep Learning
* Encryption
* Generative Models
* Reinforcement Learning
* Genetic Algorithms
* Unsupervised Learning
* _Not using Machine Learning in the first place_
